<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:guestbook/Resources/Private/Language/locallang_db.xlf:tx_guestbook_domain_model_message',
        'label' => 'name',
        'iconfile' => 'EXT:guestbook/Resources/Public/Icons/Message.svg'
    ],
    'columns' => [
        'uid' => [
            'label' => 'LLL:EXT:guestbook/Resources/Private/Language/locallang_db.xlf:tx_guestbook_domain_model_message.uid',
            'config' => [
                'type' => 'input'
            ]
        ],
        'name' => [
            'label' => 'LLL:EXT:guestbook/Resources/Private/Language/locallang_db.xlf:tx_guestbook_domain_model_message.name',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim'
            ]
        ],
        'message_content' => [
            'label' => 'LLL:EXT:guestbook/Resources/Private/Language/locallang_db.xlf:tx_guestbook_domain_model_message.message_content',
            'config' => [
                'type' => 'text',
                'eval' => 'trim'
            ]
        ],
        'date' => [
            'label' => 'LLL:EXT:guestbook/Resources/Private/Language/locallang_db.xlf:tx_guestbook_domain_model_message.date',
            'config' => [
                'type' => 'input',
                'eval' => 'trim'
            ]
        ]
    ],
    'types' => [
        '0' => ['showitem' => 'name, message_content']
    ]
];
