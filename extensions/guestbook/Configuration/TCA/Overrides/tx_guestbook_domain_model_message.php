<?php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    'guestbook',
    'tx_guestbook_domain_model_message',
    'categories' // this parameter is optional! default value is categories
);