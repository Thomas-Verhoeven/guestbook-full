<?php
return [
    'frontend' => [
        'guestbook-middleware' => [
            'target' => \MyVendor\Guestbook\Middleware\GuestbookMiddleware::class,
            'before' => [
                'typo3/cms-frontend/output-compression',
           ],
            'after' => [
                'typo3/cms-frontend/content-length-headers',
            ],
        ]
    ]
];