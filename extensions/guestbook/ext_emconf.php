<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Guestbook',
    'description' => 'Guestbook practice extension.',
    'category' => 'plugin',
    'author' => 'Thomas Verhoeven',
    'author_company' => 'TV Ltd',
    'author_email' => 'thomas.verhoeven@maxserv.com',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '0.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '^9.0',
        ]
    ],
    'autoload' => [
        'psr-4' => [
            'MyVendor\\Guestbook\\' => 'Classes'
        ]
    ],
];
