console.log('Avast ye landlubbers!');

// Deprecated; eID replaced by making use of typenum. Code works so keeping it for future reference
// function testRequest() {
//     fetch('/index.php?eID=ajax_page&action=test')
//         // .then(response => response.json())
//         .then(data => console.log(data));
// }

function testRequest(uid) {
    fetch('/index.php?type=123&delete=' + uid)
        .then(response => response.json())
        .then(data => console.log(data));
}

function deleteMessage(uid) {
    if (confirm('Are you sure you want to delete this message?')) {
        fetch('/index.php?type=123&delete=' + uid)
            .then(response => response.json())
            .then(function(response) {
                alert(response);
                window.location.reload(true);
            });
    }
}

// Deprecated; Leaving here for future reference
// function editMessage(uid = 5) {
//     fetch('/?eID=ajax_page')
//         .then(response => response.json())
//         .then(function(response) {
//             console.log(response);
//             document.getElementById('messageFormName').value = response['messageName'];
//             document.getElementById('messageFormMessageContent').value = response['messageContent'];
//             var form = document.getElementsByClassName('GuestbookTable');
//             form[0].scrollIntoView();
//         });
// }

function editMessage(uid) {
    fetch('/index.php?type=456&edit=' + uid)
        .then(response => response.json())
        .then(function(response) {
            console.log(response);
            document.getElementById('messageFormName').value = response['messageName'];
            document.getElementById('messageFormMessageContent').value = response['messageContent'];
            document.getElementById('messageFormUid').value = response['uid'];
            var form = document.getElementsByClassName('GuestbookTable');
            form[0].scrollIntoView();
        });
}