<?php

namespace MyVendor\Guestbook\Domain\Repository;

/**
 * Class ProductRepository
 *
 * @package MyVendor\Guestbook\Domain\Repository
 */
class MessageRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function __construct(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        parent::__construct($objectManager);

        /* Overrides the default query settings; disables page id checking
         This allows messages to be read by the repository regardless of pid of the storage folder.
        */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
    }
}
