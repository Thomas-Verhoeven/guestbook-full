<?php

namespace MyVendor\Guestbook\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Message extends AbstractEntity
{
    /**
     * The category of the message
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category>
     */
    protected $categories;

    /**
     * The name of the commenter
     * @var string
     **/
    protected $name;

    /**
     * The message content in the guestbook
     * @var string
     **/
    protected $messageContent;

    /**
     * The date of the message
     * @var string
     **/
    protected $date;

    /**
     * Message constructor.
     * TODO add param for category
     * @param string $name
     * @param string $messageContent
     * @param string $date
     */
    public function __construct($category = null, $name = 'DefaultName', $messageContent = 'DefaultMessage', $date = null)
    {
        $date = $date ?? (new \DateTime())->format('Y-m-d H:i:s');
        $this->setName($name);
        $this->setMessageContent($messageContent);
        $this->setDate($date);
    }

    /**
     * @return ObjectStorage
     */
    public function getCategories(): ObjectStorage
    {
        return $this->categories;
    }

    /**
     * @param ObjectStorage $categories
     */
    public function setCategories(ObjectStorage $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Sets the name of the commenter
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Gets the name of the commenter
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the message content
     * @param string $messageContent
     */
    public function setMessageContent(string $messageContent)
    {
        $this->messageContent = $messageContent;
    }

    /**
     * Gets the message content
     * @return string
     */
    public function getMessageContent()
    {
        return $this->messageContent;
    }

    /**
     * Sets the date of the comment
     * @param string $date
     */
    public function setDate(string $date)
    {
        $this->date = $date;
    }

    /**
     * Gets the date of the comment
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

}
