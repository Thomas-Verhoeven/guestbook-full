<?php

namespace MyVendor\Guestbook\Controller;

use MyVendor\Guestbook\Domain\Model\Message;
use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class FormController
 *
 * @package MyVendor\Guestbook\Controller
 */
class FormController extends ActionController
{

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * @var PersistenceManager
     */
    private $persistenceManager;

    /**
     * @var MessageController
     */
    private $messageController;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectMessageRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * Inject the persistence manager
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager $persistenceManager
     */
    public function injectPersistenceManager(PersistenceManager $persistenceManager)
    {
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * Inject the message controller
     *
     * @param \MyVendor\Guestbook\Controller\MessageController $messageController
     */
    public function injectMessageController(MessageController $messageController)
    {
        $this->messageController = $messageController;
    }

    /**
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */

    public function validateFormAction() {
//        $request = $this->request->getArguments();
//        if (isset($request['guestbook']['name']) && isset($request['guestbook']['messageContent'])) {
////            echo '<pre>';
////            var_dump($request);
////            echo $request['guestbook']['uid'];
////            if (is_numeric($request['guestbook']['uid'])) {
////                echo 'Jep';
////            }
////            die();
//            $name = $request['guestbook']['name'];
//            $content = $request['guestbook']['messageContent'];
//            if (isset($request['guestbook']['uid']) && is_numeric($request['guestbook']['uid'])) {
//                $uid = $request['guestbook']['uid'];
//                $this->messageController->editMessageAction($uid, $name, $content);
//            } else {
//                $this->messageController->addMessageAction($name, $content);
//            }
//            $this->redirectToUri('/', null, 200);
//        }
    }
}
