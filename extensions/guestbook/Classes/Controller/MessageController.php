<?php
namespace MyVendor\Guestbook\Controller;

//use TYPO3\CMS\Core\Database\ConnectionPool;
//use TYPO3\CMS\Core\Utility\GeneralUtility;
use phpDocumentor\Reflection\Types\Object_;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use MyVendor\Guestbook\Domain\Model\Message;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class MessageController extends ActionController
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectMessageRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    // Called by FormController
    public function addMessageAction($name, $messageContent)
    {
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $querySettings = $objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $test = $querySettings->getStoragePageIds();
//        echo '<pre>';
//        var_dump($test);
//        die('Test');
        $this->messageRepository->setDefaultQuerySettings($querySettings);

        $message = new Message($category, $name, $messageContent);
        $this->messageRepository->add($message);
//        die('Hallo');
        return 'Message has been added';
    }

//    Deprecated; using eID instead for practice
//    public function editMessageAction($uid) {
//        $data = print_r($this->request);
////        $response = json_encode('Test');
//        $response = json_encode($data);
//        return $response;
//    }

    // Called by FormController
    public function editMessageAction($uid, $name, $content)
    {
//        die('Edit');
        $message = $this->messageRepository->findByUid($uid);
        $message->setName($name);
        $message->setMessageContent($content);
        $this->messageRepository->update($message);

        $response = json_encode('Message has been updated');
        return $response;
    }

    // Called through typenum page 456
    public function getMessageAction() {
        $uri = $this->request->getRequestUri();
        preg_match("/edit=([0-9]+)/", $uri, $matches);
        if (!key_exists(1, $matches)) {
            return json_encode('Missing argument');
        }
        $uid = $matches[1];

        $message = $this->messageRepository->findByUid($uid);
        if ($message) {
            $response = [];
//            var_dump($message);
            $response['messageName'] = $message->getName();
            $response['messageContent'] = $message->getMessageContent();
            $response['uid'] = $uid;
            $response = json_encode($response);
        } else {
            $response = json_encode('Message not found');
        }
        return $response;
    }

    // Called through typenum page 123
    public function deleteMessageAction()
    {
        $uri = $this->request->getRequestUri();
        preg_match("/delete=([0-9]+)/", $uri, $matches);
        if (!key_exists(1, $matches)) {
            return json_encode('Missing argument');
        }
        $uid = $matches[1];

//        Deprecated; Keeping the code for future reference
//        GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_guestbook_domain_model_message')
//            ->delete(
//                'tx_guestbook_domain_model_message', // from
//                [ 'uid' => (int)$uid ] // where
//        );

        $message = $this->messageRepository->findByUid($uid);
        if ($message) {
            $this->messageRepository->remove($message);
            $response = json_encode('Message has been deleted');
        } else {
            $response = json_encode('Message not found');
        }

        return $response;
    }
}

