<?php
namespace MyVendor\Guestbook\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

use MyVendor\Guestbook\Domain\Repository\MessageRepository;


/**
 * Class MyEidController
 * @package MyVendor\Guestbook\Controller
 *
 * Deprecated
 * Choice of eID over form submission came from educational reasons.
 * Practical application is low since it doesn't refresh the page to indicate changes
 *
 * Use of eID has been replaced with typenum. This class only serves as future reference
 */

class MyEidController
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectMessageRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * @param ResponseInterface $response
     */
    public function deleteMessage(ServerRequestInterface $request) : ResponseInterface
    {
        if (is_int($toBeDeletedMessage = $request->getQueryParams()['delete'])) {
            GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_guestbook_domain_model_message')
                ->delete(
                    'tx_guestbook_domain_model_message', // from
                    [ 'uid' => (int)$toBeDeletedMessage ] // where
                );
        }
        $response = new JsonResponse();
        return $response;
    }

    public function getEditMessage(ServerRequestInterface $request)
    {
        $params = $request->getQueryParams();

        $messageUid = $params['uid'];
        // Most of the backend isnt loaded for eID calls.
        // It would be very ugly to try and use it to return a message from the database
        $message = 'See comment';

        $message = 'Message has been updated';
        $response = new JsonResponse($message, 200);

        return $response;
    }
}
