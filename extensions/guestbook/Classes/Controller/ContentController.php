<?php

namespace MyVendor\Guestbook\Controller;

use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class ContentController
 *
 * @package MyVendor\Guestbook\Controller
 */
class ContentController extends ActionController
{

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectProductRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * List Action
     *
     * @return void
     */
    public function showContentAction()
    {
        $this->listMessages();
//        $this->TestGuestbook();
    }

    public function listMessages()
    {

        $test = $this->objectManager;
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $test = $querySettings;
        $querySettings->setRespectStoragePage(true);
        $querySettings->setStoragePageIds([3]);
//        echo '<pre>';
//        var_dump($test);
//        die('Test');
        $this->messageRepository->setDefaultQuerySettings($querySettings);

        $messages = $this->messageRepository->findAll();

//        TODO Verify added and edited messages are in the database
        $this->view->assign('messages', $messages);
    }

    public function TestGuestbook() {
        $this->view->assign('TestGuestbook', 'Hey hoi');
    }
}
