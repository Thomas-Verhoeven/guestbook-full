<?php
namespace MyVendor\Guestbook\Middleware;

use MyVendor\Guestbook\Controller\MessageController;
use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class GuestbookMiddleware implements MiddlewareInterface
{
    /**
     * Adds an instance of TYPO3\CMS\Core\Http\NormalizedParams as
     * attribute to $request object
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $category = 3;
        $table = 'tx_guestbook_domain_model_message';
        $collection = \TYPO3\CMS\Frontend\Category\Collection\CategoryCollection::load(
            $category,
            TRUE,
            $table
        );
        echo '<pre>';
//        var_dump($collection);
        if ($collection->count() > 0) {
            // Add items to the collection of records for the current table
            foreach ($collection as $item) {
                var_dump($item);
                $tableRecords[$item['uid']] = $item;
                // Keep track of all categories a given item belongs to
                if (!isset($categoriesPerRecord[$item['uid']])) {
                    $categoriesPerRecord[$item['uid']] = array();
                }
                $categoriesPerRecord[$item['uid']][] = $aCategory;
            }
        }
        var_dump($tableRecords);

//        $this->pageId = $GLOBALS['TSFE']->id;
//        echo $this->pageId;
        die();
        // Collects the data from the form
        $message = $request->getParsedBody()['guestbook'];
        $name = $message['name'];
        $content = $message['messageContent'];
        // The minimum data to process the form is name and content. If missing, this middleware will pass the request
        if ($name != null && $content != null) {
            // Creates the required classes for handling the request
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
            $messageRepository = GeneralUtility::makeInstance(MessageRepository::class, $objectManager);
            $messageRepository->injectPersistenceManager($persistenceManager);
            $messageController = GeneralUtility::makeInstance(MessageController::class);
            $messageController->injectMessageRepository($messageRepository);

            // Picks the right method based on whether uid is set or not
            $uid = $message['uid'];
            if (is_numeric($uid)) {
                $messageController->editMessageAction($uid, $name, $content);
            } else {
                $messageController->addMessageAction($name, $content);
            }
            $persistenceManager->persistAll();
        }
//        $test = $request->getParsedBody();
//        echo '<pre>';
//        echo $name, '<br>';
//        echo $content, '<br>';
//        var_dump($test);
//        die();
        return $handler->handle($request);
    }
}
