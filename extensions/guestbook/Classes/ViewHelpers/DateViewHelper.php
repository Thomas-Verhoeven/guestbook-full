<?php

namespace MyVendor\Guestbook\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class DateViewHelper extends AbstractViewHelper
{
    protected $escapeOutput = false;

    // Should no longer be necessary after v9
//    use CompileWithRenderStatic;

    public function initializeArguments()
    {
        $this->registerArgument('date', 'string', 'The date to be displayed', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $date = new \DateTime($arguments['date']);
        $dateDiv  = '<div class="messageDate">';
        $dateDiv .= '<div class="messageDateMonth">';
        $dateDiv .= $date->format('M');
        $dateDiv .= '</div>';
        $dateDiv .= '<div class="messageDateDay">';
        $dateDiv .= $date->format('j');
        $dateDiv .= '</div>';
        $dateDiv .= '</div>';

        return $dateDiv;
    }
}