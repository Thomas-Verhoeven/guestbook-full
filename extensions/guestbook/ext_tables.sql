CREATE TABLE tx_guestbook_domain_model_message (
	uid int(11) unsigned DEFAULT '0' NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
	message_content text DEFAULT 'Hoi' NOT NULL,
	date datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid)
);
