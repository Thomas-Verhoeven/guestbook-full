Assignment to learn about and practice with TYPO3 v10

Not all implemented code makes sense or has the right/efficient approach.
The main goal was learning different ways of doing things and exploring a broad spectrum
of options instead of focussing on one specific implementation.

For further information, ask the author.

Installation: 

Run 'composer install' in root directory
Run 'npm install' in Build/Sass directory
    Run 'npm run scss' in Build/Sass

Run 'ddev config' to configure ddev
Import database dump found at Build/db.sql.gz in ddev
Run 'ddev start'
